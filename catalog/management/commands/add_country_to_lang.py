# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.management.base import BaseCommand

from catalog.models import TranslateLanguage


class Command(BaseCommand):

    def handle(self, *args, **options):
        for o in TranslateLanguage.objects.all():
            country_code = settings.LANGUAGES_ISO_CONVERT.get(o.slug)
            if country_code:
                o.country_code = country_code
                o.save()
