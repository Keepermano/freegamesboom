# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from tqdm import tqdm
import re

from catalog.models import Category, Tag, Game


class Command(BaseCommand):

    def handle(self, *args, **options):

        models = (Category, Tag, Game)
        for model in models:
            for o in tqdm(model.objects.all(), disable=False):
                try:
                    del_text = re.search(r'<p>(.*?)</p>', o.description).group(0)
                    o.description = o.description.replace(del_text, '').strip()
                    o.save()
                except (AttributeError, IndexError, TypeError):
                    pass
                try:
                    del_text = re.search(r'<p>(.*?)</p>', o.description_ru).group(0)
                    o.description_ru = o.description_ru.replace(del_text, '').strip()
                    o.save()
                except (AttributeError, IndexError, TypeError):
                    pass
                try:
                    del_text = re.search(r'<p>(.*?)</p>', o.description_ar).group(0)
                    o.description_ar = o.description_ar.replace(del_text, '').strip()
                    o.save()
                except (AttributeError, IndexError, TypeError):
                    pass
