# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import get_language


class PublicLanguageManager(models.Manager):

    def get_queryset(self):
        lang = get_language().replace('-', '_')
        exclude = {'name_{lang}__isnull'.format(lang=lang): True}
        order_field = 'name_%s' % lang
        return super().get_queryset().filter(public=True).exclude(**exclude).order_by('position', order_field)

        # return super().get_queryset().filter(public=True).exclude(
        #     **exclude
        # ).annotate(
        #     total_games=Count('game', filter=Q(game__public=True), exclude=Q(**exclude)),
        # ).exclude(
        #     total_games=0
        # ).order_by(
        #     'position', order_field
        # )


class PublicTranslateLanguageManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().filter(active=True, processed=True)
